using System.Windows;
using SciChart.Charting.Visuals;
using SciChart.Examples.ExternalDependencies.Controls.ExceptionView;

namespace SciChartExport
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
		     DispatcherUnhandledException += App_DispatcherUnhandledException;

			 InitializeComponent();

            // TODO: Put your SciChart License Key here if needed
            // Set this code once in App.xaml.cs or application startup
            SciChartSurface.SetRuntimeLicenseKey("0NmF4YPyFSrYN2HnhPB3+BxawMxZH+JGeFAyWdNhk8j8LDW+Kh40YfN1xN02wGXXSGB0RigyuoKtAAGyGJ1/ThdTeOkssiaNucpaMmtlxjhdpIcy6S3TYEBCAAQuXFLjAv+/wRP92UA3UChYqJ0UeCdOv40qfp+YyXCJ6LH8BcMco96wdTFxpi2davyqhLZex+8RFps3RgcwEzv+OwHZu2JJEl/RrFwv52chbFIr+SoHwgUK52y1+4Fyt4qQIiQkJeeU9ZYyr/xguJtxXkRvslTMy30rKkJoPz/sqtRHvKBjPBmyMbXC8U4ogmbCyJkuSN1kIjR/5uyEeO/JxgHEMCvhvbEbuhfeyu5haYWLt+TY86Q6P5ggfXtPoiBH2zO1ZFu3MZO8A6UfH9JkdcKnZsuf6pGAzzqG0cu/X6ZRU/OliszqwN1+X3vG/Wo1l6xs+3+97awGEuze+aC3k9FyT3cVSOo+mrGmJs9EmVVncRRpGZN7RzJsuBBTuN/ArZBnWhxJwDTwiLgul/5I5u9Kd4CgAPny7vs6U8PtmauX2CdG+vlkhXEAQZGcdo78EiM=");
        }

		private void App_DispatcherUnhandledException(object sender,
            System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {            
            var exceptionView = new ExceptionView(e.Exception)
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
            };
            exceptionView.ShowDialog();

            e.Handled = true;
        }
    }
}
