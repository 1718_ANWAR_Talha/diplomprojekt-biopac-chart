using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using SciChart.Charting.Model.DataSeries;
using SciChart.Charting.Visuals.RenderableSeries;
using SciChart.Data.Model;
using SciChart.Examples.ExternalDependencies.Data;

namespace SciChart.Examples.Examples.CreateSimpleChart
{
    public partial class EMGChartView : UserControl
    {
        // hier wird die Variable deklariert, die im FileChooserDialog ausgewählt wurde als einzulesendes EMG-CSV
        public static string filename;


        public EMGChartView()
        {
            InitializeComponent();
        }

        // diese Methode sorgt dafür, dass entweder die Sekunden oder mV einer ganzen Zeile im CSV zurückgegeben werden
        // dafür wird zuerst die Zeile weitergegeben an die Methode und dann die Position (bei 0 wird die Sekunde zurückgegeben und bei 1 die mV)
        private double ConvertLineToDouble(string line, int position)
        {
            return double.Parse((line.Split(';')[position]).Replace('.', ','));
        }

        private void LineChartExampleView_OnLoaded(object sender, RoutedEventArgs e)
        {
            // bei einem leeren Filenamen passiert nix, Exception wird vermieden!
            if (string.IsNullOrWhiteSpace(filename))
            {
                
            }
            else
            {
                // damit werden alle Zeilen der CSV-Datei in ein String-Array eingelesen 
                string[] lines = File.ReadAllLines(filename);

                try
                {
                    // erstellen von einem X-Y-Paar, welches die Datenquelle der EMG-Linie bildet
                    var dataSeries = new XyDataSeries<double, double>();

                    // erstellen einer Übergangskollektion, die beim Einlesen der Werte befüllt wird
                    Dictionary<double, double> keyValues = new Dictionary<double, double>();

                    lineRenderSeries.DataSeries = dataSeries;

                    // Schleife durch das String-Array (Zeile 1 wird übersprungen, weil Titel der Zeilen)
                    foreach (string l in lines.Skip(1))
                    {
                        // Die Zeile mit Position 0 bzw. Position 1 wird übergeben an die Hilfsmethode ConvertLineToDouble, um die Sekunden und mV zu markieren
                        double secD = ConvertLineToDouble(l, 0);
                        double mvD = ConvertLineToDouble(l, 1);
                        // Die Werte werden als X-Y-Kombination zur Übergangskollektion hinzugefügt
                        keyValues.Add(secD, mvD);
                    }
                    // sortieren der Übergangskollektion nach den Sekunden
                    keyValues.OrderBy(x => x.Key);

                    // dieses if sorgt dafür, dass je mehr Zeilen eingelesen werden, desto länger dauert die Animation, damit es realistisch ausschaut bei der Animation
                    if (lines.Length < 150)
                    {
                        sweepanimation.Duration = TimeSpan.FromSeconds(5);
                    }
                    else if (lines.Length < 500)
                    {
                        sweepanimation.Duration = TimeSpan.FromSeconds(10);
                    }
                    else if (lines.Length < 1000)
                    {
                        sweepanimation.Duration = TimeSpan.FromSeconds(15);
                    }
                    else if (lines.Length >= 1000)
                    {
                        sweepanimation.Duration = TimeSpan.FromSeconds(20);
                    }

                    // wenn die Übergangskollektion vollständig befüllt ist, werden die Daten in die X-Y-Kollektion übertragen
                    foreach (KeyValuePair<double, double> entry in keyValues)
                    {
                        dataSeries.Append(entry.Key, entry.Value);
                    }
                    // setzen des sichtbaren Bereichs der X-/Y-Achse um 1,5-fach größer als den tatsächlichen Höchst-/Tiefwert, damit die Darstellung besser ausschaut
                    xAxis.VisibleRange.SetMinMax(keyValues.Keys.Min() * 1.5, keyValues.Keys.Max() * 1.5);
                    yAxis.VisibleRange.SetMinMax(keyValues.Values.Min() * 1.5, keyValues.Values.Max() * 1.5);
                }
                // bei einem Fehler wird er in der Console ausgegeben, damit man ihn erkennt
                catch (Exception ef)
                {
                    Console.WriteLine(ef.StackTrace + "\n" + ef.InnerException);
                    MessageBox.Show("Beim Einlesen der Datei gab es einen Fehler, überprüfen Sie nochmal das Format der Datei!", "Fehler!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }


        }

        // diese Methode sorgt dafür, dass sobald die Animation fertig ist, wird automatisch das Sichtfenster angepasst an die EKG-Werte 
        private void Sweepanimation_AnimationEnded(object sender, EventArgs e)
        {
            sciChart.AnimateZoomExtents(TimeSpan.FromSeconds(1.5));
        }

        // diese Methode sorgt dafür, dass falls keine Datei ausgewählt wird, auch keine Animation gestartet wird
        private void Sweepanimation_AnimationStarted(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(filename))
            {
                sweepanimation.StopAnimation();
            }
        }
    }
}
