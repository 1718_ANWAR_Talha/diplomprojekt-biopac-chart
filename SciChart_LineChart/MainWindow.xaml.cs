using Microsoft.Win32;
using SciChart.Examples.Examples.CreateSimpleChart;
using SciChart_LineChart;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SciChartExport
{
    /// <summary>
    /// Interaction logic for Shell.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            // EKG/EMG-Tab werden deaktiviert und Schriftfarbe wird auf Grau eingestellt (damit sie ausgegraut sind und nicht anklickbar)
            ((TabItem)tabcontrol.Items.GetItemAt(1)).IsEnabled = false;
            ((TabItem)tabcontrol.Items.GetItemAt(1)).Foreground = Brushes.Gray;
            ((TabItem)tabcontrol.Items.GetItemAt(2)).IsEnabled = false;
            ((TabItem)tabcontrol.Items.GetItemAt(2)).Foreground = Brushes.Gray;
        }

        // interne Methode f�r Umstellen der Tab-Styles und wechseln des Tabs auf EKG
        private void ShowEKGChart(string filename)
        {
            // falls das File aus dem Pfad der EKG-Load-Methode existiert, dann wird im EKG-Chart das ausgew�hlte File �bergeben, damit es eingelesen werden kann
            if (File.Exists(filename)) EKGChartView.filename = filename;

            // deaktivieren des EMG-Tab und aktivieren des EKG-Tab
            ((TabItem)tabcontrol.Items.GetItemAt(1)).IsEnabled = true;
            ((TabItem)tabcontrol.Items.GetItemAt(1)).Foreground = Brushes.Black;
            ((TabItem)tabcontrol.Items.GetItemAt(2)).IsEnabled = false;
            ((TabItem)tabcontrol.Items.GetItemAt(2)).Foreground = Brushes.Gray;
            tabcontrol.SelectedIndex = 1;
        }

        // interne Methode f�r Umstellen der Tab-Styles und wechseln des Tabs auf EMG
        private void ShowEMGChart(string filename)
        {
            // falls das File aus dem Pfad der EMG-Load-Methode existiert, dann wird im EMG-Chart das ausgew�hlte File �bergeben, damit es eingelesen werden kann
            if (File.Exists(filename)) EMGChartView.filename = filename;

            // deaktivieren des EKG-Tab und aktivieren des EMG-Tab
            ((TabItem)tabcontrol.Items.GetItemAt(2)).IsEnabled = true;
            ((TabItem)tabcontrol.Items.GetItemAt(2)).Foreground = Brushes.Black;
            ((TabItem)tabcontrol.Items.GetItemAt(1)).IsEnabled = false;
            ((TabItem)tabcontrol.Items.GetItemAt(1)).Foreground = Brushes.Gray;
            tabcontrol.SelectedIndex = 2;
        }

        // Methode f�r den Klick auf den EKG-Laden Button
        // Hier �ffnet sich der OpenFileDialog zum Aussuchen des EKG-CSV, welches reingeladen werden soll
        // Danach wird der Pfad des im OpenFileDialog ausgesuchten Files der Hilfsmethode ShowEKGChart �bergeben
        private void EKG_load_Btnclick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "CSV-Dateien (*.csv)|*.csv";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (openFileDialog.ShowDialog() == true)
                ShowEKGChart(openFileDialog.FileName);
        }

        // Methode f�r den Klick auf den EKG-Laden Button
        // Hier �ffnet sich der OpenFileDialog zum Aussuchen des EMG-CSV, welches reingeladen werden soll
        // Danach wird der Pfad des im OpenFileDialog ausgesuchten Files der Hilfsmethode ShowEMGChart �bergeben
        private void EMG_load_Btnclick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "CSV-Dateien (*.csv)|*.csv";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (openFileDialog.ShowDialog() == true)
                ShowEMGChart(openFileDialog.FileName);
        }

        // Methode f�r den Klick auf den FAQ-Button
        // Wenn auf den FAQ-Button gedr�ckt wird, �ffnet sich das Fenster mit den Fragen & Antworten
        private void FAQ_Click(object sender, RoutedEventArgs e)
        {
            HilfeDialog hilfeDialog = new HilfeDialog();
            // durch das ShowDialog wird das FAQ-Fenster als fokussiertes Fenster bestimmt und auf das dahinter kann nicht zugegriffen werden, bis man das FAQ-Fenster nicht schlie�t
            hilfeDialog.ShowDialog();
        }
    }
}
